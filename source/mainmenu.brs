Function CreateMainMenu() As Object
    Return {
        items:["PLAY", "HOW TO PLAY", "OPTIONS", "ABOUT", "QUIT"],
        fontname:"prehistoriccaveman",
        backgroundcolor:&h000000FF,
        inactivecolor:&hebebebff,
        activecolor:&hebeb00ff,
        currentIndex:0,

        start: Function(context) As Void
        End Function,

        finish: Function (context) As Void
        End Function,

        draw: Function(context,screen) As Void
            lineHeight = context.resources.fonts[m.fontname].GetOneLineHeight() 
            screen.clear(m.backgroundcolor)
            y=0
            index=0
            for each item in m.items
                if (index=m.currentIndex)
                    screen.DrawText(item, 0, y, m.activecolor, context.resources.fonts[m.fontname])
                else
                    screen.DrawText(item, 0, y, m.inactivecolor, context.resources.fonts[m.fontname])
                end if
                index=index+1
                y = y + lineHeight
            end for
        End Function,

        handle: Function (context, event) As String
            if(type(event)="roUniversalControlEvent")
                button = event.GetInt()
                if(button=2)'up
                    m.currentIndex=(m.currentIndex+m.items.Count()-1) MOD m.items.Count()
                else if(button=3)'down
                    m.currentIndex=(m.currentIndex+1) MOD m.items.Count()
                else if(button=6)'select
                else if(button=0)'back
                end if
            end if
            Return "mainmenu"
        End Function
    }
End Function