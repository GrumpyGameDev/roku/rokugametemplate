Function CreateResources()
    resources = {}
    resources.fontRegistry = CreateObject("roFontRegistry")
    resources.fontRegistry.Register("pkg:/assets/Fluo Gums.ttf")
    resources.fontRegistry.Register("pkg:/assets/Prehistoric Caveman.ttf")
    resources.fonts = {}
    resources.fonts.default = resources.fontRegistry.GetDefaultFont()
    resources.fonts.fluogums = resources.fontRegistry.GetFont("Fluo Gums", resources.fontRegistry.GetDefaultFontSize(), false, false)
    resources.fonts.prehistoriccaveman = resources.fontRegistry.GetFont("Prehistoric Caveman", resources.fontRegistry.GetDefaultFontSize(), false, false)
    return resources
End Function